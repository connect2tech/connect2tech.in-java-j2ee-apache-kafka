# Apache Kafka

**Staring Server**

```
C:\nc\kafka_2.13-2.5.0\bin\windows>
kafka-server-start.bat ./../../config/server.properties
```

**Create Topic**

```
C:\nc\kafka_2.13-2.5.0\bin\windows>
kafka-topics.bat --create  --topic mytopic --zookeeper localhost:2181 --replication-factor 1 --partitions 1
Created topic mytopic.
```

**Display All The Topics**

```
C:\nc\kafka_2.13-2.5.0\bin\windows>kafka-topics.bat --list --zookeeper localhost:2181
my_topic
Mytopic
```
 
**Run Producer**

```
C:\nc\kafka_2.13-2.5.0\bin\windows>kafka-console-producer.bat --broker-list localhost:9092 --topic mytopic
>hello1
>hello2
```

**Run Consumer**

```
C:\nc\kafka_2.13-2.5.0\bin\windows>kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic mytopic --from-beginning
hello1
hello2
a
b
10
20
```
